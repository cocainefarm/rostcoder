use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::read_to_string;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PipeFile {
    pub pipes: Pipes,
}

impl PipeFile {
    pub fn load(path: Option<&str>) -> Result<Self> {
        Ok(match path {
            Some(path) => Self::load_custom(path).map_err(|e| {
                log::error!("Failed to read custom pipes.toml file at {}", path);
                e
            })?,
            None => Self::load_default()?,
        })
    }

    fn load_custom(path: &str) -> Result<Self> {
        let pipes_string = read_to_string(path)?;
        let data: Self = toml::from_str(&pipes_string)?;

        if !data.pipes.verify() {
            log::error!("Failed to read custom file");
            Self::load_default()
        } else {
            Ok(data)
        }
    }

    fn load_default() -> Result<Self> {
        log::info!("loading default pipes.toml file");
        let default_file = include_str!("../Pipes.toml");
        let default: Self = toml::from_str(default_file)?;
        if !default.pipes.verify() {
            Err(anyhow::Error::msg("Failed to read custom file"))
        } else {
            Ok(default)
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Pipes(HashMap<String, Pipe>);

#[derive(Debug, Deserialize, Serialize, Clone, Default)]
pub struct Pipe {
    #[serde(default = "encoder")]
    pub encoder: String,
    #[serde(default = "bitrate")]
    pub encoder_property: String,
    #[serde(default = "viewfinder")]
    pub viewfinder: String,
    #[serde(default = "modifier")]
    pub bitrate_modifier: u32,
}

fn encoder() -> String {
    "v4l2src !
    videoconvert !
    x264enc tune=zerolatency speed-preset=3 key-int-max=60 name=venc_kbps !
    h264parse config-interval=-1 ! queue max-size-time=10000000000 max-size-buffers=1000 max-size-bytes=41943040 ! mux.
    alsasrc device=hw:2 ! identity name=a_delay signal-handoffs=TRUE ! volume volume=1.0 !
    audioconvert ! opusenc bitrate=160000 ! aacparse ! queue max-size-time=10000000000 max-size-buffers=1000 ! mux.
    mpegtsmux name=mux !
    appsink name=appsink".to_string()
}

fn viewfinder() -> String {
    "queue ! videoconvert ! jpegenc ! image/jpeg,width=480,height=270 ! multipartmux ! appsink name=viewfinder"
        .to_string()
}

fn bitrate() -> String {
    "bitrate".to_string()
}

fn modifier() -> u32 {
    1
}

impl Pipes {
    pub fn new(pipes: HashMap<String, Pipe>) -> Self {
        Self(pipes)
    }

    pub fn verify(&self) -> bool {
        let mut r = true;
        for (name, b) in self.0.iter() {
            if !b.encoder.contains("appsink") {
                log::error!("Failed to find appsink in the encoder pipeline of {}", name);
                r = false;
            }
            if !b.viewfinder.contains("appsink") {
                log::error!(
                    "Failed to find appsink in the viewfinder pipeline of {}",
                    name
                );
                r = false;
            }
        }
        r
    }

    pub fn get(&self, name: &str) -> Option<&Pipe> {
        self.0.get(name)
    }

    pub fn default(&self) -> Pipe {
        Pipe::default()
    }

    pub fn get_inner_values(&self) -> &HashMap<String, Pipe> {
        &self.0
    }

    pub fn merge(&mut self, other: Self) {
        self.0.extend(other.0);
    }
}
