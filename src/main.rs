mod config;
mod encoder;
mod error;
mod pipelines;
#[cfg(feature = "viewfinder")]
mod viewfinder;

use anyhow::Result;
use chrono::{FixedOffset, Timelike, Utc};
use clap::Parser;
use std::{thread, time};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long)]
    config: Option<String>,
}

fn main() -> Result<()> {
    pretty_env_logger::init();
    let args = Args::parse();
    //let args: Vec<String> = env::args().collect();
    let config = config::Config::get_from_file(args.config)?;

    //let boards = boards::Boards::load(None)?;
    //let board = boards.get(&config.board).unwrap(); //.unwrap_or(&boards.default());
    //create encoder with channel
    let (mut encoder, tx, rx) = encoder::Encoder::new(config.clone())?;

    let now = Utc::now().with_timezone(&FixedOffset::east(7200));
    log::info!("Started: {}:{}:{}", now.hour(), now.minute(), now.second());
    //move the encoder in another thread
    thread::spawn(move || encoder.run());

    //start the encoder
    tx.send(encoder::Msg::Start)?;

    //start optional debug websocket
    #[cfg(feature = "ws")]
    let ws_tx = {
        use tokio::net::{TcpListener, TcpStream};
        use tokio::runtime::Builder;

        let rt = Builder::new_current_thread().enable_io().build()?;
        let (ws_tx, ws_rx) = tokio::sync::broadcast::channel(16);
        let ws_tx2 = ws_tx.clone();
        thread::spawn(move || {
            rt.block_on(async {
                // moved to there, to prevent ws+viewfinder clash
                let addr = "0.0.0.0:4444";
                let socket = TcpListener::bind(&addr)
                    .await
                    .map_err(|e| {
                        log::error!("Failed to bind addres for ws: {:?}", e);
                        e
                    })
                    .unwrap();
                log::debug!("Ws listening on: {}", addr);

                // Let's spawn the handling of each connection in a separate task.
                while let Ok((stream, addr)) = socket.accept().await {
                    let rx2 = ws_tx2.subscribe();
                    tokio::spawn(handle_connection(stream, addr, rx2));
                }
                println!("end");
            });
        });
        ws_tx
    };

    #[cfg(feature = "gps")]
    {
        let tx2 = tx.clone();
        thread::spawn(move || update_gps(tx2));
    }

    //recive msg form the encoder
    while let Ok(msg) = rx.recv() {
        #[cfg(feature = "ws")]
        ws_tx.send(msg.clone());

        match msg {
            //restart the encoder if it gets disconnected/errors out
            encoder::Msg::Error(error::EncoderError::SrtError(srt_error)) => {
                tx.send(encoder::Msg::Disconnected)?;

                log::error!("Error occured: {}", srt_error);
                if config.auto_connect {
                    // auto reconnect delay
                    let wait = time::Duration::from_millis(config.auto_connect_delay);
                    log::info!("Waiting...");
                    thread::sleep(wait);
                    tx.send(encoder::Msg::Start)?;
                }
            }
            encoder::Msg::Stop => break,
            _ => {}
        }
    }
    Ok(())
}

#[cfg(feature = "ws")]
async fn handle_connection(
    raw_stream: tokio::net::TcpStream,
    addr: std::net::SocketAddr,
    mut rx: tokio::sync::broadcast::Receiver<encoder::Msg>,
) {
    use futures_channel::mpsc::{unbounded, UnboundedSender};
    use futures_util::{future, pin_mut, stream::TryStreamExt, SinkExt, StreamExt};
    use tokio_tungstenite::tungstenite::Message;

    log::info!("Incomming connection form: {}", addr);

    let mut ws_stream = tokio_tungstenite::accept_async(raw_stream)
        .await
        .expect("Error during the websocket handshake occurred");
    println!("WebSocket connection established: {}", addr);
    while let Ok(msg) = rx.recv().await {
        let text = format!("{:?}", msg);
        ws_stream.send(Message::Text(text)).await.unwrap();
    }
}

#[cfg(feature = "gps")]
fn update_gps(tx: std::sync::mpsc::Sender<encoder::Msg>) -> Result<()> {
    use std::time::Duration;

    let mut port = serialport::new("/dev/ttyTHS1", 9_600)
        .timeout(Duration::from_millis(30))
        .open()
        .expect("Failed to open port");

    let mut serial_buf: Vec<u8> = vec![0; 32];
    let mut buf: Vec<u8> = Vec::new();
    let mut sentence = false;
    loop {
        port.read(serial_buf.as_mut_slice());

        for item in &serial_buf {
            if item == &b'$' {
                sentence = true;
                buf.push(*item);
            } else if item == &b'\r' {
                if let Ok(s) = std::str::from_utf8(&buf) {
                    if s.contains("GPVTG") {
                        let (_, s) = s.split_at(6);
                        let s: Vec<&str> = s.split(',').collect();
                        let mut speed = s[7].parse().unwrap_or(0.0);
                        if speed < 1.0 {
                            speed = 0.0;
                        }
                        let msg = format!("{:.2} km/h", speed);
                        tx.send(encoder::Msg::CustomText(msg));
                        log::debug!("{} km/h", speed);
                    }
                }
                sentence = false;
                buf = Vec::new();
            } else if sentence {
                buf.push(*item);
            }
        }
    }

    Ok(())
}

//#[cfg(feature = "viewfinder")]
//pub async fn run(pipeline: gst::Pipeline) {
//    let (tx, _) = tokio::sync::broadcast::channel(1);
//
//    let (vtx, mut vrx) = tokio::sync::broadcast::channel(1);
//
//    let tx2 = tx.clone();
//    tokio::spawn(async move {
//        let mut running = false;
//
//        let mut bin = None;
//
//        while let Ok(msg) = vrx.recv().await {
//            if msg && !running {
//                let tx3 = tx2.clone();
//                bin = Some(run_viewfinder(&pipeline, tx3).unwrap());
//                running = true;
//            } else if running {
//                if let Some((b, pad)) = &bin {
//                    stop_viewfinder(&pipeline, b, pad).unwrap();
//                }
//                running = false;
//            }
//        }
//    });
//
//    //TODO add to config
//    let web_addr = SocketAddr::from(([0, 0, 0, 0], 8080));
//
//    let web_service = make_service_fn(move |_| {
//        let tx2 = tx.clone();
//        let vtx2 = vtx.clone();
//        async { Ok::<_, Infallible>(service_fn(move |req| web(req, tx2.subscribe(), vtx2.clone() ))) }
//    });
//
//    let server = Server::bind(&web_addr)
//        .http1_preserve_header_case(true)
//        .http1_title_case_headers(true)
//        .serve(web_service);
//
//    log::debug!("Listening on http://{}", web_addr);
//
//    if let Err(e) = server.await {
//        log::debug!("server error: {}", e);
//    }
//}
//
//async fn web(req: Request<Body>, rx: Receiver<Vec<u8>>, tx: Sender<bool>) -> Result<Response<Body>> {
//    let index = include_str!("./viewfinder/index.html");
//    match (req.method(), req.uri().path()) {
//        (&Method::GET, "/stop") => {
//            tx.send(false).unwrap();
//            Ok(Response::default())
//        },
//        (&Method::GET, "/start") => {
//            tx.send(true).unwrap();
//            Ok(Response::default())
//        },
//        (&Method::GET, "/video.mjpg") => {
//            serve_req(rx).await
//        },
//        _ => {
//            let body = Body::from(index);
//            let resp = Response::builder()
//                .body(body)
//                .unwrap();
//            Ok(resp)
//        }
//    }
//}
//
//async fn serve_req(rx: Receiver<Vec<u8>>) -> Result<Response<Body>> {
//    let rx = BroadcastStream::new(rx);
//    let result_stream = rx.map(|buffer| hyper::Result::Ok(buffer.unwrap_or(Vec::new())) );
//    let body = Body::wrap_stream(result_stream);
//
//    Ok(Response::builder()
//       .status(StatusCode::OK)
//       .header("Content-Type", "multipart/x-mixed-replace; boundary=--7b3cc56e5f51db803f790dad720ed50a") // MJPEG stream.
//       .body(body)
//       .unwrap())
//}
