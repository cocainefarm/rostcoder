use anyhow::Result;
use chrono::{FixedOffset, Timelike, Utc};
use gst::prelude::*;
use gst::traits::ElementExt;
use gst::{element_error, element_info};
use srt_rs::SrtSocket;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time::Instant;

#[cfg(feature = "viewfinder")]
use tokio::sync::broadcast::Sender as TokioSender;

use crate::config::Config;
use crate::error::*;
use crate::pipelines::Pipe;
#[cfg(feature = "viewfinder")]
use crate::viewfinder::Viewfinder;

mod buffistory;
use buffistory::HistoryBuffer;

static AUDIO_BITRATE: i32 = 160000;
static PIPELINE_STALL_CHECK: u128 = 1500; //in ms

#[derive(Clone, Debug)]
pub enum Msg {
    Start,
    Starting,
    Stop,
    #[cfg(feature = "viewfinder")]
    ViewfinderStart(TokioSender<Vec<u8>>),
    #[cfg(feature = "viewfinder")]
    ViewfinderStop,
    Kill,
    Restart,
    Disconnected,
    BitrateUpdate(u32),
    MaxBitrateUpdate(u32),
    MinBitrateUpdate(u32),
    ConfigUpdate(Config),
    CustomText(String),
    FlipAudio,
    Mute(bool),
    Audio(bool),
    AudioChannels(Vec<u8>),
    Error(EncoderError),
    Stalled,
}

pub struct Encoder {
    rx: Receiver<Msg>,
    tx: Sender<Msg>,
    config: Config,
    pipeline: Option<gst::Pipeline>,
    bus: Option<gst::Bus>,
    appsink_update: Instant,
    appsink_tx: Sender<Msg>,
    restart: Option<Instant>,
    viewfinder: Option<Viewfinder>,
    viewfinder_update: Instant,
    send_audio: bool,
}

impl Encoder {
    pub fn new(config: Config) -> Result<(Self, Sender<Msg>, Receiver<Msg>)> {
        let (outer_tx, rx) = channel();
        let (tx, outer_rx) = channel();
        let (appsink_tx, _) = channel();

        let encoder = Self {
            rx,
            tx,
            config,
            pipeline: None,
            bus: None,
            appsink_update: Instant::now(),
            appsink_tx,
            restart: None,
            viewfinder: None,
            viewfinder_update: Instant::now(),
            send_audio: false,
        };
        Ok((encoder, outer_tx, outer_rx))
    }

    pub fn run(&mut self) -> Result<()> {
        loop {
            if let Err(e) = self.on_tick_msg() {
                log::error!("on msg tick: {:?}", e);
                self.stop_pipeline()?;
            }
            if let Err(e) = self.on_tick_bus() {
                log::error!("on bus tick: {:?}", e);
                self.stop_pipeline()?;
            }
            if let Some(restart) = self.restart {
                if restart.elapsed().as_millis() >= 2000 {
                    self.tx.send(Msg::Restart)?;
                    self.stop_pipeline()?;
                    thread::sleep(std::time::Duration::from_millis(1000));
                    self.start_pipeline()?;
                    self.restart = None;
                }
            }
            thread::sleep(std::time::Duration::from_millis(20));
        }
    }

    fn on_tick_msg(&mut self) -> Result<()> {
        if let Ok(msg) = self.rx.try_recv() {
            match msg {
                Msg::Start => {
                    self.restart = None;
                    self.start_pipeline()?;
                }
                Msg::Disconnected => {
                    //self.running = false;
                    //srt_rs::cleanup()?;
                }
                Msg::Stop => {
                    //self.restart = None;
                    self.stop_restart()?;
                    self.stop_pipeline()?;

                    //NOTE if no pipeline is running no msg is send
                    self.tx.send(Msg::Stop)?;
                }
                Msg::Restart => {
                    self.restart = Some(Instant::now());
                }
                Msg::ConfigUpdate(config) => {
                    self.config = config;
                }
                #[cfg(feature = "viewfinder")]
                Msg::ViewfinderStart(tx) => {
                    if self.viewfinder.is_none() {
                        if let Some(pipeline) = &self.pipeline {
                            let pipeline2 = pipeline.clone();
                            self.viewfinder = Some(Viewfinder::start(
                                pipeline2,
                                tx,
                                &self.config.get_pipeline(),
                            )?);
                            self.viewfinder_update = Instant::now();
                        }
                    }
                }
                #[cfg(feature = "viewfinder")]
                Msg::ViewfinderStop => self.stop_viewfinder()?,
                Msg::Audio(sending) => self.send_audio = sending,
                Msg::Mute(mute) => self.mute(mute),
                Msg::FlipAudio => self.flip_audio(),
                _ => {
                    self.appsink_tx.send(msg)?;
                }
            }
        }
        Ok(())
    }

    fn on_tick_bus(&mut self) -> Result<()> {
        if let Some(bus) = &self.bus {
            if self.appsink_update.elapsed().as_millis() >= PIPELINE_STALL_CHECK {
                log::debug!("Pipeline taking to long, killing .....");
                self.tx.send(Msg::Stalled)?;
                self.stop_pipeline()?;
                return Ok(());
            }

            let mut bus_iter = bus.iter();
            if let Some(msg) = bus_iter.next() {
                use gst::MessageView;
                match msg.view() {
                    MessageView::Info(info) => {
                        if let Some(src) = info.src() {
                            let src = format!("{}", src);
                            match src.as_str() {
                                "viewfinder" => {
                                    let subs: i32 = info.error().message().parse().unwrap();
                                    if subs == 0
                                        && self.viewfinder_update.elapsed().as_millis() >= 125
                                    {
                                        self.stop_viewfinder()?;
                                    } else if subs > 0 {
                                        self.viewfinder_update = Instant::now();
                                    }
                                }
                                "appsink" => {
                                    self.appsink_update = Instant::now();
                                }
                                _ => {}
                            }
                        }
                    }
                    MessageView::Eos(..) => self.stop_pipeline()?,
                    MessageView::Error(err) => {
                        let err = err.error().kind();
                        match err {
                            Some(EncoderError::Stop) => {}
                            Some(_) => {
                                self.tx.send(Msg::Error(err.unwrap()))?;
                            }
                            _ => {}
                        }
                        self.stop_pipeline()?;
                    }
                    MessageView::Element(element) => {
                        if element.has_name("level") && self.send_audio {
                            if let Some(level_struc) = element.structure() {
                                let rms = level_struc.value("rms")?;
                                let channels = rms.get::<gst::glib::ValueArray>()?;
                                let channels = channels
                                    .iter()
                                    .map(|c| (c.get().unwrap_or(0.0) * -1.0) as u8)
                                    .collect::<Vec<u8>>();
                                self.tx.send(Msg::AudioChannels(channels))?;
                            }
                        }
                    }
                    _ => {}
                }
            }
        }
        Ok(())
    }

    fn stop_restart(&mut self) -> Result<()> {
        if self.restart.is_some() {
            self.restart = None;
            self.tx.send(Msg::Stop)?;
        }

        Ok(())
    }

    fn stop_pipeline(&mut self) -> Result<()> {
        #[cfg(feature = "viewfinder")]
        self.stop_viewfinder()?;

        if let Some(pipeline) = &self.pipeline {
            log::info!("stopping pipeline");
            pipeline.set_state(gst::State::Null)?;
            self.tx.send(Msg::Stop)?;
            srt_rs::cleanup()?;
            self.pipeline = None;
            self.bus = None;
            self.tx.send(Msg::Stop)?;
        }

        Ok(())
    }

    fn start_pipeline(&mut self) -> Result<()> {
        //NOTE only start pipeline if none is running
        if self.pipeline.is_none() {
            self.tx.send(Msg::Starting)?;
            log::debug!("Starting pipeline. "); // TODO Add datetime
            srt_rs::startup()?;
            let (appsink_tx, appsink_rx) = channel();
            self.appsink_tx = appsink_tx;
            let tx2 = self.tx.clone();

            //TODO error handling
            match create_pipeline(&self.config, tx2, appsink_rx) {
                Ok(pipeline) => match pipeline.set_state(gst::State::Playing) {
                    Ok(_) => {
                        self.bus = Some(pipeline.bus().unwrap());
                        self.pipeline = Some(pipeline);
                        self.appsink_update = Instant::now();
                        self.tx.send(Msg::Start)?;
                    }
                    Err(e) => {
                        log::error!("error starting pipeline: {:?}", e);
                        self.tx.send(Msg::Error(EncoderError::PipelineError))?;
                    }
                },
                Err(e) => {
                    log::error!("error starting pipeline: {:?}", e);
                    self.tx.send(Msg::Error(EncoderError::PipelineError))?;
                }
            }
        }
        Ok(())
    }

    fn stop_viewfinder(&mut self) -> Result<()> {
        log::debug!("stop viewfinder");
        if let Some(v) = &self.viewfinder {
            v.stop_viewfinder()?;
            self.viewfinder = None;
        }
        Ok(())
    }

    fn mute(&mut self, mute: bool) {
        if let Some(pipe) = &self.pipeline {
            if let Some(vol) = pipe.by_name("vol") {
                vol.set_property("mute", mute);
            }
        }
    }

    fn flip_audio(&mut self) {
        if let Some(pipe) = &self.pipeline {
            if let Some(flipper) = pipe.by_name("flipper") {
                let ltl = flipper.property::<f64>("left-to-left");
                let ltr = flipper.property::<f64>("left-to-right");
                let rtl = flipper.property::<f64>("right-to-left");
                let rtr = flipper.property::<f64>("right-to-right");

                flipper.set_property("left-to-left", ltr);
                flipper.set_property("left-to-right", ltl);
                flipper.set_property("right-to-left", rtr);
                flipper.set_property("right-to-right", rtl);
            }
        }
    }
}

fn create_pipeline(
    config: &Config,
    tx: Sender<Msg>,
    rx: Receiver<Msg>,
) -> Result<gst::Pipeline, EncoderError> {
    log::debug!("creating pipeline");
    gst::init()?;

    let pipeline = gst::Pipeline::new(None);

    let pipe_config = config.get_pipeline();
    let pipe = pipe_config.encoder.to_string();
    log::debug!("using pipe: {}", pipe);

    #[cfg(feature = "viewfinder")]
    {
        let (src, pipe) = pipe.split_once('!').ok_or({
            log::error!("Failed to modify pipeline for viewfinder");
            EncoderError::PipelineError
        })?;
        let encoder_pipe = gst::parse_bin_from_description(pipe, true)?;

        let mut src_and_prop: Vec<&str> = src.split_ascii_whitespace().collect();
        let src_name = src_and_prop.remove(0);
        let src = gst::ElementFactory::make(src_name, Some("src"))?;
        for prop in src_and_prop {
            if let Some((name, prop)) = prop.split_once('=') {
                src.set_property(name, prop);
            }
        }
        let tee = gst::ElementFactory::make("tee", Some("t"))?;

        pipeline.add_many(&[&src, &tee])?;
        pipeline.add(&encoder_pipe)?;
        src.link(&tee)?;
        let templ = tee.pad_template("src_%u").unwrap();
        let teepad = tee.request_pad(&templ, None, None).unwrap();
        let encoder_sink = encoder_pipe.static_pad("sink").unwrap();
        teepad.link(&encoder_sink).unwrap();
    }

    #[cfg(not(feature = "viewfinder"))]
    {
        let pipe = gst::parse_launch(&pipe)?;
        pipeline.add(&pipe)?;
    }
    let now = Utc::now().with_timezone(&FixedOffset::east(7200));
    log::info!("Started: {}:{}:{}", now.hour(), now.minute(), now.second());

    //get textoverlay
    let overlay = pipeline.by_name("bitrate");

    let custom = pipeline.by_name("custom");

    //get appsink
    let sink = pipeline.by_name("appsink").ok_or({
        log::error!("missing name 'appsink' for the appsink");
        gst::FlowError::CustomError1
    })?;

    //TODO remove kbps and change name to something more making sense like "encoder" or "enc", the bitrate modifire is allredy set
    //get encoder
    let (vencoder, _bps) = match pipeline.by_name("venc_bps") {
        Some(pipe) => (pipe, 1),
        None => {
            let pipe = pipeline
                .by_name("venc_kbps")
                .ok_or(gst::FlowError::CustomError1)?;
            (pipe, 1000)
        }
    };
    //set default bitrate on encoder
    vencoder.set_property(
        &pipe_config.encoder_property,
        config.max_bitrate / pipe_config.bitrate_modifier,
    );

    // TODO finish adding the variable audio br
    let aencoder = pipeline.by_name("audioenc").ok_or({
        log::error!("missing name 'audioenc' in pipeline for audio encoder");
        gst::FlowError::CustomError1
    })?;
    aencoder.set_property("bitrate", AUDIO_BITRATE);

    let appsink = sink
        .dynamic_cast::<gst_app::AppSink>()
        .expect("Sink element is expected to be an appsink!");

    //create srt socket
    let sock = srt_rs::SrtSocket::new()?;
    sock.set_max_bandwith(0)?;
    sock.set_recovery_bandwidth_overhead(20)?;
    sock.set_latency(config.latency)?;
    sock.set_stream_id(&config.stream_id)?;
    sock.set_retransmission_algorithm(true)?;
    sock.set_max_reorder_tolerance(10)?;

    if let Some(filter) = &config.srt_filter {
        sock.set_packet_filter(filter)?;
    }

    sock.connect(&config.ip)?;

    let time = std::time::Instant::now();
    let mut mpeg_buffer = Vec::new();
    //  let mut next_bitrate_check = 0;
    let mut bitrate = Bitrate::new(config, &pipe_config);

    let mut next_check: u64 = 0;

    // fun fact: this callback is called between 330 and 4053
    // (assuming lower bandwidth bound of 496kbps, upper of 6096kbps)
    // times per second
    appsink.set_callbacks(
        gst_app::AppSinkCallbacks::builder()
            .new_sample(move |appsink| {
                // check for msg
                match rx.try_recv() {
                    Ok(Msg::Stop) => {
                        element_error!(appsink, EncoderError::Stop, ("Srt Error"));
                        return Err(gst::FlowError::Flushing);
                    }
                    Ok(Msg::MaxBitrateUpdate(b)) => {
                        log::info!("Updating max bitrate: {}", b);
                        bitrate.update_max_bitrate(b);
                        //config.max_bitrate = bitrate;
                    }
                    Ok(Msg::MinBitrateUpdate(b)) => {
                        log::info!("Updateing min bitrate: {}", b);
                        bitrate.update_min_bitrate(b);
                        //config.min_bitrate = bitrate;
                    }
                    Ok(Msg::CustomText(txt)) => {
                        if let Some(c) = &custom {
                            c.set_property("text", txt);
                        }
                    }
                    _ => {}
                }

                let ctime = time.elapsed().as_millis() as u64;
                if ctime > next_check {
                    element_info!(appsink, gst::ResourceError::Write, ("keep alive"));
                    // it's significantly wonkier
                    next_check = ctime + 100;

                    match bitrate.update_bitrate(ctime, &sock, &vencoder, &aencoder, &overlay) {
                        Ok(Some(msg)) => tx.send(msg).unwrap(),
                        Ok(None) => {}
                        Err(e) => {
                            log::error!("{:?}", e);
                        }
                    }
                }

                // Pull the sample in question out of the appsink's buffer.
                let sample = appsink.pull_sample().map_err(|_| gst::FlowError::Eos)?;
                let buffer = sample.buffer().ok_or_else(|| {
                    element_error!(
                        appsink,
                        gst::ResourceError::Failed,
                        ("Failed to get buffer from appsink")
                    );

                    gst::FlowError::Error
                })?;

                mpeg_buffer.append(
                    &mut buffer
                        .map_readable()
                        .map_err(|_| {
                            element_error!(
                                appsink,
                                gst::ResourceError::Failed,
                                ("Failed to map buffer readable")
                            );

                            gst::FlowError::Error
                        })?
                        .to_vec(),
                );

                if mpeg_buffer.len() >= 1316 {
                    let srt_buffer: Vec<_> = mpeg_buffer.drain(..1316).collect();
                    if let Err(e) = sock.send(srt_buffer.as_slice()) {
                        element_error!(
                            appsink,
                            EncoderError::SrtError(EncoderSrtError { inner: e }),
                            //gst::ResourceError::Failed,
                            ("Srt Error")
                        );
                        sock.close();
                        return Err(gst::FlowError::CustomError);
                    }
                }
                //                }
                Ok(gst::FlowSuccess::Ok)
            })
            .build(),
    );
    Ok(pipeline)
}

struct Bitrate {
    max_bitrate: u32,
    min_bitrate: u32,
    cur_bitrate: u32,
    pipe: Pipe,
    rtt_hist: HistoryBuffer<f64>,
    pkt_flight_hist: HistoryBuffer<i32>,
    retrans_hist: HistoryBuffer<i32>,
    sendloss_hist: HistoryBuffer<i32>,
    bw_hist: HistoryBuffer<u32>,
}

impl Bitrate {
    fn new(config: &Config, pipe: &Pipe) -> Self {
        Self {
            max_bitrate: config.max_bitrate * 1000,
            min_bitrate: config.min_bitrate * 1000,
            // start out at 40% max, it'll get to a good point soon
            cur_bitrate: config.max_bitrate * 600,
            pipe: pipe.to_owned(),
            rtt_hist: HistoryBuffer::<f64>::with_capacity(120, 10),
            pkt_flight_hist: HistoryBuffer::<i32>::with_capacity(180, 5),
            retrans_hist: HistoryBuffer::<i32>::with_capacity(180, 10),
            sendloss_hist: HistoryBuffer::<i32>::with_capacity(180, 10),
            bw_hist: HistoryBuffer::<u32>::with_capacity(120, 10),
        }
    }

    fn update_max_bitrate(&mut self, bitrate: u32) {
        self.max_bitrate = bitrate * 1000;
    }

    fn update_min_bitrate(&mut self, bitrate: u32) {
        self.min_bitrate = bitrate * 1000;
    }

    fn _next_update(&mut self) -> u64 {
        // bits per byte per mpeg packet per frame per second
        // the theory on this could be more accurate than a fixed timer
        // hoping to get "a frame" better. a fixed timer could miss the
        // window, a frame gets encoded at a bit rate than the pipe is
        // kinda able to send
        //
        // (nb, this should be updated from self.framerate, which should be
        // enumerated from the pipeline itself. super useful
        // when variable framerate is ready - just another pipeline step)
        // there's also some "futz" to this rn.
        // also needs the audio bitrate for accurate measurement
        // bits / bytes / mpeg frames / quarter frames per second (for 60fps)
        ((self.cur_bitrate + AUDIO_BITRATE as u32) / 8 / 188 / 15) as u64
    }

    fn update_bitrate(
        &mut self,
        ctime: u64,
        sock: &SrtSocket,
        encoder: &gst::Element,
        aencoder: &gst::Element,
        overlay: &Option<gst::Element>,
    ) -> Result<Option<Msg>> {
        let stats = sock.bistats()?;
        let rtt = stats.msRTT;
        if rtt == 100.0 && !self.rtt_hist.is_ready() {
            self.bw_hist.push(self.cur_bitrate);
            return Ok(None);
        }

        self.rtt_hist.push(rtt);
        self.pkt_flight_hist.push(stats.pktFlightSize);
        self.retrans_hist.push(stats.pktRetrans);
        self.sendloss_hist.push(stats.pktSndLoss);
        // things used in only once in calc, and logs
        let pkt_avg_delta = self.pkt_flight_hist.mean_delta();
        let retrans_avg_delta = self.retrans_hist.mean_delta();
        let sendloss_delta = self.sendloss_hist.mean_delta();

        let mut bitrate = self.bw_hist.ewma() as u32;
        //  the baseline
        //  the retransmit/loss will try to reduce the bandwidth to reduce failures
        let zeropoint = 0.75
            - ((retrans_avg_delta.clamp(0.0, 4.0) * 1.5) + (sendloss_delta.clamp(0.0, 4.0) * 1.6));
        let change =
            ((zeropoint - (pkt_avg_delta * 0.8)) * (self.max_bitrate / 20) as f64).trunc() as i32;

        bitrate =
            (bitrate as i32 + change.clamp(-(bitrate as i32), self.max_bitrate as i32)) as u32;
        bitrate = bitrate.clamp(self.min_bitrate, self.max_bitrate);

        // change is actually clamped to decrease by twice the minimum bw
        //
        log::debug!(
            "{:6.3}\t{:>5}\t{:>5.0}\t{:>5.0}\t{:>5.0}\t{:>3}\t{:>1.3}\t{:>3}\t{:>4}\t{:>4}\t{:>4.2}\t{:>4.2}\t{:>4.2}",
            ctime as f64 / 1000.0, // {:6.3}
            bitrate / 1000, // {:>5}
            change / 1000, // {:5.0}
            self.bw_hist.ewma() / 1000.0, // {:5.0}
            stats.mbpsSendRate * 1000.0, // {:5.0}
            stats.pktRetrans, // {:>3}
            zeropoint,  // {:>3} TODO REMOVE THIS
            stats.pktSndLoss, // {:>3}
            stats.pktSent, // {:>4}
            stats.pktFlightSize, // {:>4}
            self.pkt_flight_hist.ewma(), // {:>4.2}
            pkt_avg_delta, // {:4.2}
            rtt, // {:4.2}
        );
        // should bitrate be bps internally, and not kbps? idk
        self.bw_hist.push(bitrate);
        if bitrate != self.cur_bitrate {
            self.cur_bitrate = bitrate;

            // this needs improving.
            let audio_ratio =
                AUDIO_BITRATE as f32 * (0.7 + (self.bw_hist.ewma() / 1000000.0) as f32);
            let audiobr = (audio_ratio as i32).clamp(AUDIO_BITRATE / 5, AUDIO_BITRATE);
            aencoder.set_property("bitrate", audiobr);
            encoder.set_property(
                &self.pipe.encoder_property,
                bitrate / self.pipe.bitrate_modifier,
            );

            //Set bitrate on overlay
            if let Some(o) = overlay {
                o.set_property("text", format!("{}k", bitrate / 1000));
            }

            // amending the bitrate message to reflect the target
            return Ok(Some(Msg::BitrateUpdate(self.bw_hist.ewma() as u32 / 1000)));
        }
        Ok(None)
    }
}
