use anyhow::{Error, Result};
use serde::{Deserialize, Serialize};
use std::fs::read_to_string;
use std::path::{Path, PathBuf};
use toml_edit::{Document, value};

use crate::pipelines::{Pipe, PipeFile, Pipes};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Config {
    pub ip: String,
    pub stream_id: String,
    pub max_bitrate: u32,
    pub min_bitrate: u32,
    pub latency: i32,
    pub auto_connect: bool,
    #[serde(default)]
    pub auto_connect_delay: u64,
    #[serde(default)]
    pub bitrate_overlay: bool,
    #[serde(default)]
    pub custom_overlay: bool,
    pub srt_filter: Option<String>,
    #[serde(default)]
    pub overlay: Option<String>,
    #[serde(default)]
    pub pipe_name: String,

    pub pipes: Pipes,
}

impl Config {
    pub fn get_from_file(path: Option<String>) -> Result<Self> {
        let path = match path {
            Some(p) => Path::new(&p).to_path_buf(),
            None => get_base_path()?,
        };
        log::info!("loading config file");
        log::debug!("config path: {:?}", path.canonicalize());
        let string = read_to_string(path).map_err(|_| Error::msg("Config file not found"))?;
        let mut config: Self = toml::from_str(&string)?;

        let default_pipes = PipeFile::load(None)?;
        if !config.pipes.verify() {
            panic!("Error in the custom pipelines");
        }
        config.pipes.merge(default_pipes.pipes);

        log::debug!("Config: {:#?}", config);

        Ok(config)
    }

    pub fn get_pipeline(&self) -> Pipe {
        match self.pipes.get(&self.pipe_name) {
            Some(pipe) => pipe.to_owned(),
            None => Pipe::default(),
        }
    }

    pub fn save_config(&self) -> Result<()> {
        let path = get_base_path()?;
        let string = read_to_string(&path)?;
        let mut document: Document = string.parse()?;

        document["ip"] = value(&self.ip);
        document["max_bitrate"] = value(self.max_bitrate as i64);
        document["min_bitrate"] = value(self.min_bitrate as i64);
        document["stream_id"] = value(&self.stream_id);
        document["pipe_name"] = value(&self.pipe_name);

        log::info!("saving config file");
        log::debug!("config path: {:?}", path.canonicalize());
        std::fs::write(path, document.to_string())?;
        Ok(())
    }
}

fn get_base_path() -> Result<PathBuf> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("rostcoder")?;
    Ok(xdg_dirs.place_config_file("config.toml")?)
}
