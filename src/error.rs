use gst::glib::error::ErrorDomain;
use gst::glib::BoolError;
use srt_rs::error::SrtError;
use thiserror::Error;

#[derive(Copy, Clone, Debug, Error)]
pub enum EncoderError {
    #[error("SrtError '{0}'")]
    SrtError(#[from] EncoderSrtError),
    #[error("Gstreamer flow error '{0}'")]
    FlowError(gst::FlowError),
    #[error("BoolError")]
    BoolError,
    #[error("GlibError")]
    GlibError,
    #[error("UnknownError")]
    Unknown,
    #[error("Stop")]
    Stop,
    #[error("Failed to start pipeline")]
    PipelineError,
}

impl From<gst::FlowError> for EncoderError {
    fn from(err: gst::FlowError) -> Self {
        Self::FlowError(err)
    }
}

impl From<BoolError> for EncoderError {
    fn from(_: BoolError) -> Self {
        Self::BoolError
    }
}

impl From<SrtError> for EncoderError {
    fn from(err: SrtError) -> Self {
        Self::SrtError(EncoderSrtError { inner: err })
    }
}

impl From<gst::glib::Error> for EncoderError {
    fn from(_: gst::glib::Error) -> Self {
        Self::GlibError
    }
}

#[derive(Copy, Clone, Debug, Error)]
#[error("{inner}")]
pub struct EncoderSrtError {
    pub inner: SrtError,
}

impl gst::glib::error::ErrorDomain for EncoderSrtError {
    fn domain() -> gst::glib::Quark {
        gst::glib::Quark::from_str("Encoder error")
    }

    fn code(self) -> i32 {
        match self.inner {
            SrtError::Unknown => 4,
            SrtError::Success => 5,
            SrtError::ConnSetup => 6,
            SrtError::NoServer => 7,
            SrtError::ConnRej(_) => 8,
            SrtError::SockFail => 9,
            SrtError::SecFail => 10,
            SrtError::Closed => 11,
            SrtError::ConnFail => 12,
            SrtError::ConnLost => 13,
            SrtError::NoConn => 14,
            SrtError::Resource => 15,
            SrtError::Thread => 16,
            SrtError::NoBuf => 17,
            SrtError::SysObj => 18,
            SrtError::File => 19,
            SrtError::InvRdOff => 20,
            SrtError::RdPerm => 21,
            SrtError::InvWrOff => 22,
            SrtError::WrPerm => 23,
            SrtError::InvOp => 24,
            SrtError::BoundSock => 25,
            SrtError::ConnSock => 26,
            SrtError::InvParam => 27,
            SrtError::InvSock => 28,
            SrtError::UnboundSock => 29,
            SrtError::NoListen => 30,
            SrtError::RdvNoServ => 31,
            SrtError::RdvUnbound => 32,
            SrtError::InvalMsgApi => 33,
            SrtError::InvalBufferApi => 34,
            SrtError::DupListen => 35,
            SrtError::LargeMsg => 36,
            SrtError::InvPollId => 37,
            SrtError::PollEmpty => 38,
            SrtError::AsyncFail => 39,
            SrtError::AsyncSnd => 40,
            SrtError::AsyncRcv => 41,
            SrtError::Timeout => 42,
            SrtError::Congest => 43,
            SrtError::PeerErr => 44,
        }
    }

    fn from(code: i32) -> Option<Self>
    where
        Self: Sized,
    {
        let err = match code {
            4 => Some(SrtError::Unknown),
            5 => Some(SrtError::Success),
            6 => Some(SrtError::ConnSetup),
            7 => Some(SrtError::NoServer),
            8 => Some(SrtError::ConnRej(srt_rs::error::SrtRejectReason::Unknown)),
            9 => Some(SrtError::SockFail),
            10 => Some(SrtError::SecFail),
            11 => Some(SrtError::Closed),
            12 => Some(SrtError::ConnFail),
            13 => Some(SrtError::ConnLost),
            14 => Some(SrtError::NoConn),
            15 => Some(SrtError::Resource),
            16 => Some(SrtError::Thread),
            17 => Some(SrtError::NoBuf),
            18 => Some(SrtError::SysObj),
            19 => Some(SrtError::File),
            20 => Some(SrtError::InvRdOff),
            21 => Some(SrtError::RdPerm),
            22 => Some(SrtError::InvWrOff),
            23 => Some(SrtError::WrPerm),
            24 => Some(SrtError::InvOp),
            25 => Some(SrtError::BoundSock),
            26 => Some(SrtError::ConnSock),
            27 => Some(SrtError::InvParam),
            28 => Some(SrtError::InvSock),
            29 => Some(SrtError::UnboundSock),
            30 => Some(SrtError::NoListen),
            31 => Some(SrtError::RdvNoServ),
            32 => Some(SrtError::RdvUnbound),
            33 => Some(SrtError::InvalMsgApi),
            34 => Some(SrtError::InvalBufferApi),
            35 => Some(SrtError::DupListen),
            36 => Some(SrtError::LargeMsg),
            37 => Some(SrtError::InvPollId),
            38 => Some(SrtError::PollEmpty),
            39 => Some(SrtError::AsyncFail),
            40 => Some(SrtError::AsyncSnd),
            41 => Some(SrtError::AsyncRcv),
            42 => Some(SrtError::Timeout),
            43 => Some(SrtError::Congest),
            44 => Some(SrtError::PeerErr),
            _ => None,
        };
        err.map(|err| EncoderSrtError { inner: err })
    }
}

impl gst::message::MessageErrorDomain for EncoderError {}

impl gst::glib::error::ErrorDomain for EncoderError {
    fn domain() -> gst::glib::Quark {
        gst::glib::Quark::from_str("Encoder error")
    }

    fn code(self) -> i32 {
        match self {
            Self::FlowError(_) => 0,
            Self::BoolError => 1,
            Self::GlibError => 2,
            Self::Unknown => 3,
            Self::SrtError(err) => err.code(),
            Self::Stop => 45,
            Self::PipelineError => 46,
        }
    }

    fn from(code: i32) -> Option<Self>
    where
        Self: Sized,
    {
        match code {
            0 => Some(Self::FlowError(gst::FlowError::CustomError)),
            1 => Some(Self::BoolError),
            2 => Some(Self::GlibError),
            3 => Some(Self::Unknown),
            45 => Some(Self::Stop),
            46 => Some(Self::PipelineError),
            _ => <EncoderSrtError as ErrorDomain>::from(code).map(Self::SrtError),
        }
    }
}
