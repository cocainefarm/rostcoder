use gst::{element_info, Bin, Pad, Pipeline};

use tokio::sync::broadcast::Sender as TokioSender;

use gst::element_error;
use gst::prelude::*;
use gst::traits::ElementExt;

use anyhow::Result;

use crate::pipelines::Pipe;

static HEAD: &[u8] =
    "\r\n--7b3cc56e5f51db803f790dad720ed50a\r\nContent-Type: image/jpeg\r\nContent-Length: "
        .as_bytes();
static RNRN: &[u8] = "\r\n\r\n".as_bytes();

#[derive(Debug, Clone)]
pub struct Viewfinder {
    bin: Bin,
    pad: Pad,
    pipeline: Pipeline,
}

impl Viewfinder {
    pub fn start(pipeline: Pipeline, tx: TokioSender<Vec<u8>>, pipe: &Pipe) -> Result<Self> {
        let (bin, pad) = run_viewfinder(&pipeline, tx, pipe)?;
        Ok(Self { bin, pad, pipeline })
    }

    pub fn stop_viewfinder(&self) -> Result<()> {
        log::debug!("Stop viewfinder");
        let tee = self.pipeline.by_name("t").unwrap();
        tee.unlink(&self.bin);
        self.pipeline.remove(&self.bin).unwrap();
        self.bin.set_state(gst::State::Null)?;
        tee.release_request_pad(&self.pad);
        Ok(())
    }
}

pub fn run_viewfinder(
    pipeline: &gst::Pipeline,
    tx: TokioSender<Vec<u8>>,
    pipe: &Pipe,
) -> Result<(Bin, Pad)> {
    log::debug!("Start viewfinder");

    let tee = pipeline.by_name("t").unwrap();
    let templ = tee.pad_template("src_%u").unwrap();
    let teepad = tee.request_pad(&templ, None, None).unwrap();

    //let bin = gst::parse_bin_from_description(&bin, true)?;
    let bin = gst::parse_bin_from_description(&pipe.viewfinder, true)?;
    pipeline.add(&bin).unwrap();
    let sink = bin.static_pad("sink").unwrap();
    teepad.link(&sink).unwrap();
    bin.sync_state_with_parent()?;

    let viewfinder_sink = pipeline.by_name("viewfinder").unwrap();

    let appsink = viewfinder_sink
        .dynamic_cast::<gst_app::AppSink>()
        .expect("Sink element is expected to be an appsink!");

    let mut output_buffer = Vec::new();
    let mut in_jpeg = false;

    appsink.set_callbacks(
        gst_app::AppSinkCallbacks::builder()
            .new_sample(move |appsink| {
                let sample = appsink.pull_sample().map_err(|_| gst::FlowError::Eos)?;
                let buffer = sample.buffer().ok_or_else(|| {
                    element_error!(
                        appsink,
                        gst::ResourceError::Failed,
                        ("Failed to get buffer from appsink")
                    );

                    gst::FlowError::Error
                })?;

                let buff = &mut buffer
                    .map_readable()
                    .map_err(|_| {
                        element_error!(
                            appsink,
                            gst::ResourceError::Failed,
                            ("Failed to map buffer readable")
                        );

                        gst::FlowError::Error
                    })?
                    .to_vec();

                let mut i = 0;
                loop {
                    //buff to small
                    if i >= buff.len() || buff.len() < 2 {
                        break;
                    }
                    //first bytes of jpeg
                    if buff[i] == 0xFF && buff[i + 1] == 0xD8 {
                        in_jpeg = true;
                        output_buffer.push(buff[i]);
                        output_buffer.push(buff[i + 1]);
                        i += 2;
                    //last bytes of jpeg
                    } else if buff[i] == 0xFF && buff[i + 1] == 0xD9 {
                        in_jpeg = false;
                        output_buffer.push(buff[i]);
                        output_buffer.push(buff[i + 1]);

                        //add some headeres
                        let mut mjpeg: Vec<u8> = Vec::new();
                        mjpeg.extend_from_slice(HEAD);
                        mjpeg.extend_from_slice(output_buffer.len().to_string().as_bytes());
                        mjpeg.extend_from_slice(RNRN);
                        mjpeg.extend_from_slice(output_buffer.as_slice());
                        output_buffer.clear();
                        //tx.send(mjpeg.clone());
                        let x = tx.send(mjpeg.clone());
                        match x {
                            Ok(n) => {
                                element_info!(appsink, gst::ResourceError::Write, (&n.to_string()));
                            }
                            Err(_) => {
                                element_info!(appsink, gst::ResourceError::Write, ("0"));
                            }
                        }
                        output_buffer.clear();
                    }
                    if in_jpeg {
                        output_buffer.push(buff[i]);
                    }
                    i += 1;
                }
                Ok(gst::FlowSuccess::Ok)
            })
            .build(),
    );

    Ok((bin, teepad))
}

//pub fn stop_viewfinder(pipeline: &Pipeline, bin: &Bin, src: &Pad) -> Result<()> {
//    log::debug!("Stop viewfinder");
//    let tee = pipeline.by_name("t").unwrap();
//    tee.unlink(bin);
//    pipeline.remove(bin).unwrap();
//    bin.set_state(gst::State::Null)?;
//    tee.release_request_pad(src);
//    Ok(())
//}
