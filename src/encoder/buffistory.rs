// this needs a rework to deal with bigger numbers.
// you pump, say an i32 HistoryBuffer with a bunch of multi-million numbers
// you're going to have a bad day pretty quickly.
pub struct HistoryBuffer<T> {
    history: Vec<T>,
    deltas: Vec<T>,
    sum: T,
    delta_sum: T,
    ewmav: f64,
    alpha: f64,
}

#[allow(dead_code)]
// the new will requires a sample count
impl HistoryBuffer<f64> {
    pub fn with_capacity(samples: usize, delta_ratio: usize) -> Self {
        Self {
            history: Vec::with_capacity(samples),
            deltas: Vec::with_capacity(samples / delta_ratio),
            sum: 0.0,
            delta_sum: 0.0,
            ewmav: 0.0,
            alpha: 0.2,
        }
    }
    // length needs casting to same type as .sum....
    pub fn mean(&mut self) -> f64 {
        self.sum / self.history.len() as f64
    }

    pub fn push(&mut self, value: f64) {
        self.update_delta(value);

        if self.history.len() == self.history.capacity() {
            if let Some(value) = self.history.first() {
                self.sum -= value;
            }
            self.history.remove(0);
        }
        self.history.push(value);
        self.sum += value;
    }

    fn update_delta(&mut self, value: f64) {
        self.update_ewma(value);
        // Update the average RTT delta
        let delta_rtt = match self.history.last() {
            Some(old) => value - old,
            None => 0.0,
        };

        if self.deltas.len() == self.deltas.capacity() {
            if let Some(first) = self.deltas.first() {
                self.delta_sum -= first;
            }
            self.deltas.remove(0);
        }
        self.deltas.push(delta_rtt);
        self.delta_sum += delta_rtt;
    }

    pub fn delta(&mut self) -> f64 {
        return match self.deltas.last() {
            Some(last) => *last,
            None => 0.0,
        };
    }

    pub fn mean_delta(&mut self) -> f64 {
        self.delta_sum / self.deltas.len() as f64
    }

    pub fn is_ready(&mut self) -> bool {
        self.deltas.len() == self.deltas.capacity()
    }

    pub fn min(&mut self) -> f64 {
        self.history.iter().cloned().fold(f64::NAN, f64::min)
    }
    pub fn max(&mut self) -> f64 {
        self.history.iter().cloned().fold(f64::NAN, f64::max)
    }

    pub fn jitter(&mut self) -> f64 {
        self.max() - self.min() / 2.0
    }

    fn update_ewma(&mut self, value: f64) {
        self.ewmav = self.alpha * value + (1.0 - self.alpha) * self.ewmav
    }

    pub fn ewma(&mut self) -> f64 {
        self.ewmav
    }

    pub fn alpha(&mut self, alpha: f64) {
        self.alpha = alpha
    }
}

// TODO realistically, should iX exist as a type?
// Probably, but the uX types should almost certainly return iX
// types as delta being negative sometimes makes sense.

#[allow(dead_code)]
impl HistoryBuffer<i32> {
    pub fn with_capacity(samples: usize, delta_ratio: usize) -> Self {
        Self {
            history: Vec::with_capacity(samples),
            deltas: Vec::with_capacity(samples / delta_ratio),
            sum: 0,
            delta_sum: 0,
            ewmav: 0.0,
            alpha: 0.2,
        }
    }
    // length needs casting to same type as .sum....
    pub fn mean(&mut self) -> f64 {
        self.sum as f64 / self.history.len() as f64
    }

    pub fn push(&mut self, value: i32) {
        self.update_delta(value);

        if self.history.len() == self.history.capacity() {
            if let Some(value) = self.history.first() {
                self.sum -= value;
            }
            self.history.remove(0);
        }
        self.history.push(value);
        self.sum += value;
    }

    fn update_delta(&mut self, value: i32) {
        self.update_ewma(value as f64);
        // Update the average RTT delta
        let delta_rtt = match self.history.last() {
            Some(old) => value - old,
            None => 0,
        };

        if self.deltas.len() == self.deltas.capacity() {
            if let Some(first) = self.deltas.first() {
                self.delta_sum -= first;
            }
            self.deltas.remove(0);
        }
        self.deltas.push(delta_rtt);
        self.delta_sum += delta_rtt;
    }

    pub fn delta(&mut self) -> i32 {
        match self.deltas.last() {
            Some(last) => *last,
            None => 0,
        }
    }

    pub fn mean_delta(&mut self) -> f64 {
        self.delta_sum as f64 / self.deltas.len() as f64
    }

    pub fn is_ready(&mut self) -> bool {
        self.deltas.len() == self.deltas.capacity()
    }

    pub fn min(&mut self) -> i32 {
        *self.history.iter().min().unwrap()
    }

    pub fn max(&mut self) -> i32 {
        *self.history.iter().max().unwrap()
    }

    pub fn jitter(&mut self) -> f64 {
        (self.max() - self.min()) as f64 / 2.0
    }

    fn update_ewma(&mut self, value: f64) {
        self.ewmav = self.alpha * value + (1.0 - self.alpha) * self.ewmav
    }

    pub fn ewma(&mut self) -> f64 {
        self.ewmav
    }

    pub fn alpha(&mut self, alpha: f64) {
        self.alpha = alpha
    }
}

#[allow(dead_code)]
impl HistoryBuffer<u32> {
    pub fn with_capacity(samples: usize, delta_ratio: usize) -> Self {
        Self {
            history: Vec::with_capacity(samples),
            deltas: Vec::with_capacity(samples / delta_ratio),
            sum: 0,
            delta_sum: 0,
            ewmav: 0.0,
            alpha: 0.2,
        }
    }
    // length needs casting to same type as .sum....
    pub fn mean(&mut self) -> f64 {
        self.sum as f64 / self.history.len() as f64
    }

    pub fn push(&mut self, value: u32) {
        self.update_delta(value);

        if self.history.len() == self.history.capacity() {
            if let Some(value) = self.history.first() {
                self.sum -= value;
            }
            self.history.remove(0);
        }
        self.history.push(value);
        self.sum += value;
    }

    fn update_delta(&mut self, value: u32) {
        self.update_ewma(value as f64);
        // Update the average RTT delta
        let delta_rtt = match self.history.last() {
            Some(old) => value - old,
            None => 0,
        };

        if self.deltas.len() == self.deltas.capacity() {
            if let Some(first) = self.deltas.first() {
                self.delta_sum -= first;
            }
            self.deltas.remove(0);
        }
        self.deltas.push(delta_rtt);
        self.delta_sum += delta_rtt;
    }

    pub fn delta(&mut self) -> u32 {
        match self.deltas.last() {
            Some(last) => *last,
            None => 0,
        }
    }

    pub fn mean_delta(&mut self) -> f64 {
        self.delta_sum as f64 / self.deltas.len() as f64
    }

    pub fn is_ready(&mut self) -> bool {
        self.deltas.len() == self.deltas.capacity()
    }

    pub fn min(&mut self) -> u32 {
        *self.history.iter().min().unwrap()
    }

    pub fn max(&mut self) -> u32 {
        *self.history.iter().max().unwrap()
    }

    pub fn jitter(&mut self) -> f64 {
        (self.max() - self.min()) as f64 / 2.0
    }

    fn update_ewma(&mut self, value: f64) {
        self.ewmav = self.alpha * value + (1.0 - self.alpha) * self.ewmav
    }

    pub fn ewma(&mut self) -> f64 {
        self.ewmav
    }

    pub fn alpha(&mut self, alpha: f64) {
        self.alpha = alpha
    }
}
