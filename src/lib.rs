use anyhow::Result;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;

#[cfg(feature = "viewfinder")]
use tokio::sync::broadcast::Sender as TokioSender;
use tokio::sync::broadcast::Receiver as TokioReceiver;

mod config;
mod encoder;
mod error;
mod pipelines;
#[cfg(feature = "viewfinder")]
mod viewfinder;

pub use config::Config;
pub use encoder::Msg;
use encoder::Encoder;

#[derive(Clone, Debug)]
pub struct Rostcoder {
    tx: Sender<Msg>,
    viewfinder_tx: TokioSender<Vec<u8>>
}

impl Rostcoder {
    pub fn new(config: Config) -> Result<(Self, Receiver<Msg>, Config)> {
        //let boards = pipelines::Boards::load(None)?;
        //let board = boards.get(&config.board).unwrap(); //.unwrap_or(&boards.default());
        let (mut encoder, tx, rx) = Encoder::new(config.clone())?;
        thread::spawn(move || encoder.run());

        let (viewfinder_tx, _) = tokio::sync::broadcast::channel(1);

        let irl_streamer = Self {
            tx,
            viewfinder_tx
        };
        Ok((irl_streamer, rx, config))
    }

    //pub fn new_with_custom_boards(config: Config, boards_path: &str) -> Result<(Self, Receiver<Msg>, Config)> {
    //    let boards = pipelines::Boards::load(Some(boards_path))?;
    //    let board = boards.get(&config.board).unwrap(); //.unwrap_or(&boards.default());
    //    let (mut encoder, tx, rx) = Encoder::new(config.clone(), board)?;
    //    thread::spawn(move || encoder.run());

    //    let (viewfinder_tx, _) = tokio::sync::broadcast::channel(1);

    //    let irl_streamer = Self {
    //        tx,
    //        viewfinder_tx
    //    };
    //    Ok((irl_streamer, rx, config))
    //}

    pub fn new_default_config() -> Result<(Self, Receiver<Msg>, Config)> {
        let config = config::Config::get_from_file(None)?;
        Rostcoder::new(config)
    }

    pub fn start(&mut self) -> Result<()> {
        self.tx.send(Msg::Start)?;
        Ok(())
    }

    pub fn stop(&mut self) -> Result<()> {
        self.tx.send(Msg::Stop)?;
        Ok(())
    }

    pub fn restart(&self) -> Result<()> {
        self.tx.send(Msg::Restart)?;
        Ok(())
    }

    pub fn update_config(&mut self, config: &Config) -> Result<()> {
        config.save_config()?;
        //self.config = config.to_owned();
        self.tx.send(Msg::ConfigUpdate(config.clone()))?;
        Ok(())
    }

    pub fn update_max_bitrate(&self, bitrate: u32) -> Result<()> {
        self.tx.send(Msg::MaxBitrateUpdate(bitrate))?;
        Ok(())
    }

    pub fn update_min_bitrate(&self, bitrate: u32) -> Result<()> {
        self.tx.send(Msg::MinBitrateUpdate(bitrate))?;
        Ok(())
    }

    #[cfg(feature = "viewfinder")]
    pub fn start_viewfidner(&self) -> Result<TokioReceiver<Vec<u8>>> {
        self.tx.send(Msg::ViewfinderStart(self.viewfinder_tx.clone()))?;
        Ok(self.viewfinder_tx.subscribe())
    }

    #[cfg(feature = "viewfinder")]
    pub fn stop_viewfidner(&self) -> Result<()> {
        self.tx.send(Msg::ViewfinderStop)?;
        Ok(())
    }

    //pub fn show_bitrate(&self, bitrate_overlay: bool) -> Result<()> {
    //    Ok(())
    //}

    //pub fn show_custom_overlay(&self, custom_overlay: bool) -> Result<()> {
    //    Ok(())
    //}

    pub fn update_custom_overlay(&self, text: String) -> Result<()> {
        self.tx.send(Msg::CustomText(text))?;
        Ok(())
    }

    pub fn update_audio(&self, running: bool) -> Result<()> {
        self.tx.send(Msg::Audio(running))?;
        Ok(())
    }

    pub fn update_mute(&self, mute: bool) -> Result<()> {
        self.tx.send(Msg::Mute(mute))?;
        Ok(())
    }

    pub fn flip_audio(&self) -> Result<()> {
        self.tx.send(Msg::FlipAudio)?;
        Ok(())
    }

    //pub fn get_config(&self) -> Config {
    //    self.config.clone()
    //}

    //pub fn get_config_mut(&mut self) -> &mut Config {
    //    &mut self.config
    //}

    //get channel to get updates like errors/infos
    //pub fn get_channel(&self) -> &Receiver<Msg> {
    //    &self.rx
    //}

    //fn get_msg<T: Send>() -> Result<T>
    //{

    //}
}
