{
  nixConfig = {
    substituters = [
      "https://cache.nixos.org/"
      "https://nix-community.cachix.org"
      "https://nix.cache.vapor.systems"
    ];
    trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "nix.cache.vapor.systems-1:OjV+eZuOK+im1n8tuwHdT+9hkQVoJORdX96FvWcMABk="
    ];
  };

  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-22.11";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, fenix, crane }:
    with nixpkgs.lib;
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        pkgsArm = nixpkgs.legacyPackages.aarch64-linux;
        pkgsCross = pkgs.pkgsCross.aarch64-multiplatform-musl;

        rustToolchain = with fenix.packages.${system};
          combine [
            stable.toolchain
            targets.wasm32-unknown-unknown.stable.rust-std
            targets.aarch64-unknown-linux-musl.stable.rust-std
          ];

        buildInputs = with pkgs; [
          libclang
          gst_all_1.gstreamer
          gst_all_1.gst-plugins-base
          openssl
        ];

        nativeBuildInputs = with pkgs; [
          rustToolchain
          pkg-config
          cmake

          lld
          wasm-bindgen-cli
        ];

        # crane setup
        craneLib = crane.lib.${system}.overrideToolchain rustToolchain;

        htmlFilter = path: _type: builtins.match ".*html$" path != null;
        htmlOrCargo = path: type:
          (htmlFilter path type) || (craneLib.filterCargoSources path type);
        src = nixpkgs.lib.cleanSourceWith {
          src = ./.; # The original, unfiltered source
          filter = htmlOrCargo;
        };

        cargoArtifactsServer = craneLib.buildDepsOnly {
          inherit src buildInputs nativeBuildInputs
            OPENSSL_DIR OPENSSL_LIB_DIR
            OPENSSL_INCLUDE_DIR;

          cargoExtraArgs = "--package rostcoder";
        };

        cargoArtifactsWasm = craneLib.buildDepsOnly {
          inherit src buildInputs nativeBuildInputs
            CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER;

          cargoExtraArgs = "--lib --package rostimote";
          CARGO_BUILD_TARGET = "wasm32-unknown-unknown";

          doCheck = false;
        };

        cargoArtifactsCross = craneLib.buildDepsOnly {
          inherit src nativeBuildInputs;
          inherit (aarch64-env)
            buildInputs BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_STATIC
            OPENSSL_DIR OPENSSL_LIB_DIR TARGET_CC TARGET_CXX
            OPENSSL_CRYPTO_LIBRARY OPENSSL_INCLUDE_DIR CARGO_BUILD_TARGET
            CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER;

          cargoExtraArgs = "--package rostcoder";
        };

        env = let
          openssl = pkgs.openssl_1_1;
        in {
          BINDGEN_EXTRA_CLANG_ARGS =
            "-I${pkgs.glibc.dev}/include -I${pkgs.libclang.lib}/lib/clang/11.1.0/include";

          LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";

          OPENSSL_DIR = "${openssl}";
          OPENSSL_LIB_DIR = "${openssl.out}/lib";
          OPENSSL_CRYPTO_LIBRARY = "${openssl.out}/lib";
          OPENSSL_INCLUDE_DIR = "${openssl.dev}/include";
        };

        aarch64-env = let
          openssl = pkgsCross.pkgsStatic.openssl_1_1;
          target = "aarch64-unknown-linux-musl";
        in {
          CARGO_BUILD_TARGET = target;

          LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";

          TARGET_CC = "${pkgsCross.stdenv.cc}/bin/${target}-gcc";
          TARGET_CXX = "${pkgsCross.stdenv.cc}/bin/${target}-g++";

          CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER = "wasm-ld";
          CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER =
            "${pkgsCross.stdenv.cc}/bin/${target}-gcc";

          BINDGEN_EXTRA_CLANG_ARGS =
            "-I${pkgsArm.glibc.dev}/include -I${pkgsArm.libclang.lib}/lib/clang/11.1.0/include";

          OPENSSL_STATIC = 1;
          OPENSSL_DIR = "${openssl}";
          OPENSSL_LIB_DIR = "${openssl.out}/lib";
          OPENSSL_CRYPTO_LIBRARY = "${openssl.out}/lib";
          OPENSSL_INCLUDE_DIR = "${openssl.dev}/include";


          buildInputs = buildInputs ++ [
            openssl
            pkgsCross.gst_all_1.gstreamer
            pkgsCross.gst_all_1.gst-plugins-base
          ];
        };
      in {
        packages = {
          rostcoder = craneLib.buildPackage {
            inherit buildInputs nativeBuildInputs;
            inherit (env)
              BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_DIR OPENSSL_LIB_DIR
              OPENSSL_CRYPTO_LIBRARY OPENSSL_INCLUDE_DIR;

            pname = "rostcoder";

            src = src;
            cargoArtifacts = cargoArtifactsServer;
            cargoExtraArgs = "--package rostcoder";
          };

          rostcoder-cross = craneLib.buildPackage {
            inherit src nativeBuildInputs;
            inherit (aarch64-env)
              buildInputs BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_STATIC
              OPENSSL_DIR OPENSSL_LIB_DIR OPENSSL_CRYPTO_LIBRARY
              OPENSSL_INCLUDE_DIR CARGO_BUILD_TARGET TARGET_CC TARGET_CXX
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER;

            pname = "rostcoder";
            cargoArtifacts = cargoArtifactsCross;

            cargoExtraArgs = "--package rostcoder";
          };
        };

        devShells = {
          default = pkgs.mkShell {
            inherit buildInputs nativeBuildInputs;
            inherit (env)
              BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_DIR OPENSSL_LIB_DIR
              OPENSSL_CRYPTO_LIBRARY OPENSSL_INCLUDE_DIR;
          };

          cross = pkgsCross.mkShell {
            inherit nativeBuildInputs;
            inherit (aarch64-env)
              buildInputs BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_STATIC
              OPENSSL_DIR OPENSSL_LIB_DIR OPENSSL_CRYPTO_LIBRARY
              OPENSSL_INCLUDE_DIR CARGO_BUILD_TARGET
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER;
          };
        };
      });
}
